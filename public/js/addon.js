/* add-on script */
var JWT_TOKEN;

$(document).ready(function () {
    JWT_TOKEN = $('#token').attr('content');

    $(document.body).on('click', ".delete-btn", function (event) {
        var id = this.parentElement.parentElement.getAttribute('id');
        deleteRow(id);
    });

    $.ajaxSetup({
        headers: {'Authorization': "JWT " + JWT_TOKEN}
    });

    $( document ).ajaxSuccess(function( event, xhr, settings ) {
        refreshToken(xhr);
    });

    function refreshToken(jqXHR) {
        JWT_TOKEN = jqXHR.getResponseHeader('X-acpt');
    }

    var editView = AJS.RestfulTable.CustomEditView.extend({
        render: function () {
            var that = this;
            var selected = function(category) {
                var picked = that.model.attributes.category;
                if (category == picked) {
                    return 'selected="selected"';
                }
                return "";
            };

            return $(
                '<select name="category" id="category_val" class="select">' +
                '<option value="Testers" ' + selected("Testers")+ '>Testers</option>' +
                '<option value="Developers" '+ selected("Developers")+'>Developers</option>' +
                '</select>'
            );
        }
    });

    new AJS.RestfulTable({
        autoFocus: true,
        el: jQuery("#project-config-versions-table"),
        allowReorder: false,
        resources: {
            all: "teams",
            self: "teams"
        },
        columns: [
            {
                id: "value",
                header: "Name"
            },
            {
                id: "category",
                header: "Category",
                editView : editView
    }
    ]
});
});
