# Issue Field Connect Demo

This Connect add-on makes use of an issue field module in JIRA. It adds a new entity called Team which can be managed by an administrator.

It adds:

- a general page which displays all defined teams, which are fetched from the add-ons database
- a jira project admin panel, which displays all teams which are linked to the project which is being administered
- an issue field that can be used to link a team with an issue

## To start the add-on

- Follow the [instructions](https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html) to get a dev instance
- Install package dependencies with `npm install`
- Follow the [instructions](https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally-ngrok.html) to install the add-on on the dev instance