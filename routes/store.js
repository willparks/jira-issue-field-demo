/**
 * Created by bartlomiejlewandowski on 19/05/16.
 */

var exports = module.exports = {};

var Datastore = require('nedb')
    , db = new Datastore({filename: 'data.db', autoload: true});

exports.getAllTeams = function() {
    return new Promise(function(resolve, reject) {
        db.find({}, function (err, docs) {
            console.log(err, docs);
            if (err) {
                console.error(err);
                return reject(err);
            }
            console.log('resolving');
            return resolve(docs);
        });
    });
};

exports.getTeam = function(teamId) {
    return new Promise(function(resolve, reject) {
        db.findOne({_id : teamId}, function (err, doc) {
            if (err || doc == null) {
                console.error('doc not found', err);
                return reject(err);
            }
            return resolve(doc);
        });
    });
};

exports.addTeam = function(value, category) {
    return new Promise(function(resolve, reject) {
        db.count({}, function (err, count) {
            if (err) {
                return reject(err);
            }
            var nextId = count + 1;
            var object = {
                '_id' : nextId,
                'id' : nextId,
                'value' : value,
                'category' : category
            };

            db.insert(object, function (err, newDoc) {
                if (err) {
                    return reject(err);
                }
                resolve(newDoc);
            });
        });
    });
};

exports.removeTeam = function(teamId) {
    return new Promise(function(resolve, reject) {
        db.remove({_id: teamId}, { multi: true }, function (err, numRemoved) {
            console.log(numRemoved);
            if (err || numRemoved == 0) {
                console.error('doc not found', err);
                return reject(err);
            }
            return resolve();
        });
    });
};

exports.updateTeam = function(obj) {
    return new Promise(function(resolve, reject) {
        console.log('created update promise');
        db.update({'_id': obj.id}, obj, function (err, numChanged) {
            if (err || numChanged == 0) {
                console.log('updating team failed');
                return reject();
            }
            console.log('resolving update');
            resolve(obj);
        });
    });
};
