module.exports = function (app, addon) {
    var _ = require('underscore');
    var store = require('./store.js');

    const teamFieldUrl = '/rest/api/2/field/teams-add-on__team-issue-field/option';

    var promisify = function(object, method) {
        return function () {
            var args = Array.from(arguments);
            return new Promise(function(resolve, reject) {
                var callback = function(err, resp, body) {
                    if (err) {
                        reject(err);
                    }
                    resolve(body, resp);
                };
                object[method].apply(object, [...args, callback]);
            });
        }
    };

    var getTeamsFromJIRA = function(req) {
        return promisify(addon.httpClient(req),"get")({url: teamFieldUrl})
            .then(function(body) {
                return JSON.parse(body).values;
        });
    };

    var teamsInProject = function(project, serverDocs, JIRADocs) {
        return _.map(serverDocs, function (value) {
            if (_.find(JIRADocs, function (obj) {
                    return obj['id'] == value['id'] && _.contains(obj['scope']['projects'], project);
                }) != undefined) {
                value['inProject'] = true;
            }
            return value;
        });
    };

    var toJIRAJson = function(entity){
      return {
          id: entity.id,
          value: entity.value,
          properties: {
              category: entity.category
          }
      }
    };

    var updateTeamInJIRA = function(req, newDoc) {
        var json = toJIRAJson(newDoc);
        return promisify(addon.httpClient(req), 'put')({url: teamFieldUrl + "/" + newDoc.id, json: json});
    };

    return {
        teamsInProject: function(req, projectId) {
            return store.getAllTeams().then(function(teams) {
                return getTeamsFromJIRA(req).then(function(JIRATeams) {
                    console.log('returning docs with project');
                    return teamsInProject(projectId, teams, JIRATeams);
                });
            });
        },
        addTeamToProject: function(req, teamId, projectId) {
            return store.getTeam(teamId).then(function(team) {
                var json = {
                    id : team.id,
                    value: team.value,
                    properties: {
                        category: team.category
                    },
                    scope: {
                        projects: [projectId]
                    }
                };
                return promisify(addon.httpClient(req),'put')({url: teamFieldUrl + "/" + teamId, json: json});
            });
        },

        removeTeamFromProject: function(req, teamId, projectId) {
            return store.getTeam(teamId).then(function(team) {
                return new Promise(function(resolve, reject) {
                    addon.httpClient(req).del({url: teamFieldUrl + "/" + teamId}, function(err, response, body) {
                        if (err || response.statusCode == 409) {
                            return reject(response.statusCode);
                        }
                        console.log(err, response, body);
                        resolve();
                    });
                });
            });
        },

        removeTeam: function(req, teamId) {
          return store.removeTeam(teamId);
        },

        updateTeam: function(req, teamId) {
            return store.getTeam(teamId).then(function(doc) {
                var newDoc = _.extend(doc, req.body);
                return updateTeamInJIRA(req, newDoc).then(function(jiraObj) {
                    return store.updateTeam(newDoc);
                });
            });
        }
    };
};
