module.exports = function (app, addon) {
    var sync = require('./sync.js')(app, addon);

    app.get('/teams-in-project', addon.authenticate(), function (req, res) {
        sync.teamsInProject(req, parseInt(req.query['projectId'])).then(function(result) {
            console.log('returning teams in project');
            res.render('teams-project', {
                teams: result
            });
        });
    });

    app.post('/add-team-to-project/:id', addon.checkValidToken(), function (req, res) {
        var teamId = parseInt(req.params.id);
        var projectId = req.body.value;

        sync.addTeamToProject(req, teamId, projectId).then(function() {
            res.send(200);
        }, function() {
            res.send(404);
        });
    });

    app.post('/remove-team-from-project/:id', addon.checkValidToken(), function (req, res) {
        var teamId = parseInt(req.params.id);
        var projectId = req.body.value;

        sync.removeTeamFromProject(req, teamId, projectId).then(function() {
            res.send(200);
        }, function(code) {
            res.send(code);
        });
    })
};
